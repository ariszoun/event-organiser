# Email support for sending confirmation emails when a ticket is successfully purchased.

from django.core.mail import send_mail


def send_confirmation_email(to_email, ticket_details):
    subject = 'Ticket Purchase Confirmation'
    message = f'Congratulations! You have successfully purchased the following tickets:\n\n{ticket_details}'
    from_email = 'noreply@example.com'

    send_mail(subject, message, from_email, [to_email])
