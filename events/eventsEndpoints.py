from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CategoryViewSet, CustomUserViewSet, EventViewSet, TicketViewSet, TicketCreate, PurchasedTicketsView, \
    EventSoftDelete, MockPayment

router = DefaultRouter()
router.register(r'categories', CategoryViewSet)
router.register(r'users', CustomUserViewSet)
router.register(r'events', EventViewSet)
router.register(r'tickets', TicketViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('ticket-create/', TicketCreate.as_view(), name='ticket-create'),
    path('purchased-tickets/', PurchasedTicketsView.as_view(), name='purchased-tickets'),
    path('event-soft-delete/<int:pk>/', EventSoftDelete.as_view(), name='event-soft-delete'),
    path('mock-payment/', MockPayment.as_view(), name='mock-payment'),
]
