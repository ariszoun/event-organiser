from datetime import datetime

from django.db import IntegrityError
from django.test import TestCase
from events.models import CustomUser, Category, Event, Ticket


class CRUDOperationsTestCase(TestCase):
    def setUp(self):
        self.user_host = CustomUser.objects.create_user(username="testhost", password="testpassword",
                                                        user_type=CustomUser.HOST)
        self.user_attendee = CustomUser.objects.create_user(username="testattendee", password="testpassword",
                                                            user_type=CustomUser.ATTENDEE)
        self.category = Category.objects.create(name="TestCategory")
        self.event = Event.objects.create(
            name="Test Event",
            date_and_time=datetime.now(),
            location="Test Location",
            category=self.category,
            ticket_price=100.00,
            host=self.user_host,
            description="Test Description",
            max_tickets=10,
        )
        self.ticket = Ticket.objects.create(
            event=self.event,
            attendee=self.user_attendee,
            price=100.00,
            quantity=2,
        )

    def test_create_read(self):
        self.assertEqual(self.event.name, "Test Event")
        self.assertEqual(self.ticket.quantity, 2)

    def test_update(self):
        self.event.name = "Updated Test Event"
        self.event.save()
        self.ticket.quantity = 3
        self.ticket.save()

        updated_event = Event.objects.get(id=self.event.id)
        updated_ticket = Ticket.objects.get(id=self.ticket.id)

        self.assertEqual(updated_event.name, "Updated Test Event")
        self.assertEqual(updated_ticket.quantity, 3)

    def test_delete(self):
        event_id = self.event.id
        ticket_id = self.ticket.id

        self.event.delete()
        self.ticket.delete()

        with self.assertRaises(Event.DoesNotExist):
            Event.objects.get(id=event_id)

        with self.assertRaises(Ticket.DoesNotExist):
            Ticket.objects.get(id=ticket_id)

    # creates a ticket that, when combined with the existing ticket's quantity, would cause the total quantity of
    # tickets for the event to exceed the maximum ticket limit
    def test_ticket_purchase_constraint(self):
        ticket_quantity_to_exceed_limit = self.event.max_tickets - self.ticket.quantity + 1
        with self.assertRaises(IntegrityError):
            Ticket.objects.create(
                event=self.event,
                attendee=self.user_attendee,
                price=100.00,
                quantity=ticket_quantity_to_exceed_limit,
            )

    def test_soft_delete(self):
        self.event.soft_delete()
        soft_deleted_event = Event.objects.get(id=self.event.id)
        self.assertFalse(soft_deleted_event.is_active)