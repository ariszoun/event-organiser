from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from events.models import Category, CustomUser, Event, Ticket


class CategoryTests(APITestCase):

    def test_create_category(self):
        url = reverse('category-list')
        data = {'name': 'Test Category'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Category.objects.count(), 1)
        self.assertEqual(Category.objects.get().name, 'Test Category')


class CustomUserTests(APITestCase):

    def test_create_user(self):
        url = reverse('customuser-list')
        data = {
            'username': 'testuser',
            'password': 'testpassword',
            'email': 'test@example.com',
            'user_type': 'attendee',
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CustomUser.objects.count(), 1)
        self.assertEqual(CustomUser.objects.get().username, 'testuser')


class EventTests(APITestCase):

    def test_create_event(self):
        category = Category.objects.create(name='Test Category')
        url = reverse('event-list')
        data = {
            'name': 'Test Event',
            'date_time': '2023-05-04T10:00:00Z',
            'location': 'Test Location',
            'category': category.id,
            'ticket_price': 100.0,
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Event.objects.count(), 1)
        self.assertEqual(Event.objects.get().name, 'Test Event')


class TicketTests(APITestCase):

    def test_create_ticket(self):
        event = Event.objects.create(
            name='Test Event',
            date_time='2023-05-04T10:00:00Z',
            location='Test Location',
            category=Category.objects.create(name='Test Category'),
            ticket_price=100.0,
        )
        url = reverse('ticket-list')
        data = {
            'price': 100.0,
            'quantity': 2,
            'event': event.id,
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Ticket.objects.count(), 1)
        self.assertEqual(Ticket.objects.get().quantity, 2)
