from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status


class MockPaymentTestCase(APITestCase):
    def test_mock_payment_success(self):
        # Define the test data
        price = 100.00
        quantity = 2

        # Prepare the request data
        data = {
            'price': price,
            'quantity': quantity,
        }

        # Send a POST request to the mock-payment endpoint
        url = reverse('mock-payment')
        response = self.client.post(url, data)

        # Check if the response status code is HTTP 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response contains the expected success message
        expected_message = f'Payment for {quantity} ticket(s) at ${price} each was successful.'
        self.assertEqual(response.data['message'], expected_message)
        self.assertTrue(response.data['success'])
