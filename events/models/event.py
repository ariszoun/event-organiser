from django.db import models
from .category import Category
from .custom_user import CustomUser


class Event(models.Model):
    name = models.CharField(max_length=100)
    date_and_time = models.DateTimeField()
    location = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    ticket_price = models.DecimalField(max_digits=10, decimal_places=2)
    host = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    max_tickets = models.IntegerField()

    def __str__(self):
        return self.name

    def soft_delete(self):
        self.is_active = False
        self.save()