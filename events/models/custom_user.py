from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


class CustomUser(AbstractUser):
    HOST = 'host'
    ATTENDEE = 'attendee'

    USER_TYPE_CHOICES = [
        (HOST, 'Host'),
        (ATTENDEE, 'Attendee'),
    ]

    user_type = models.CharField(
        max_length=10,
        choices=USER_TYPE_CHOICES,
        default=ATTENDEE,
    )

    groups = models.ManyToManyField(
        'auth.Group',
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions granted to each of their groups.'),
        related_name="customuser_set",
        related_query_name="customuser",
        verbose_name=_('groups'),
    )

    user_permissions = models.ManyToManyField(
        'auth.Permission',
        blank=True,
        help_text=_('Specific permissions for this user.'),
        related_name="customuser_set",
        related_query_name="customuser",
        verbose_name=_('user permissions'),
    )
