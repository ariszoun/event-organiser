from django.db import models

from events.models.custom_user import CustomUser
from events.models.event import Event


class Ticket(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    attendee = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField()

    def __str__(self):
        return f"{self.quantity} x {self.event.name}"

    class Meta:
        unique_together = ('event', 'attendee')