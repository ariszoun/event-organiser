from rest_framework import viewsets, generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Category, CustomUser, Event, Ticket
from .serializers import CategorySerializer, CustomUserSerializer, EventSerializer, TicketSerializer
from django.db.models import Sum
from django.shortcuts import get_object_or_404
from .tasks import send_confirmation_email


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CustomUserViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer


class TicketCreate(generics.CreateAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        event = get_object_or_404(Event, id=request.data['event'])
        requested_quantity = int(request.data['quantity'])
        available_tickets = event.max_tickets - Ticket.objects.filter(event=event).aggregate(Sum('quantity'))[
            'quantity__sum'] or 0
        if requested_quantity > available_tickets:
            return Response({"error": "Requested ticket quantity exceeds available tickets."},
                            status=status.HTTP_400_BAD_REQUEST)

        response = super().create(request, *args, **kwargs)
        ticket = self.get_object()
        ticket_details = f'Event: {ticket.event.name}\nQuantity: {ticket.quantity}\nTotal Price: {ticket.price}'
        send_confirmation_email(request.user.email, ticket_details)
        return response


# Handles purchased tickets for the authenticated user
class PurchasedTicketsView(generics.ListAPIView):
    serializer_class = TicketSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return Ticket.objects.filter(attendee=user)


class EventSoftDelete(generics.UpdateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        event = self.get_object()
        event.is_active = False
        event.save()
        return Response({"message": "Event has been marked as inactive."}, status=status.HTTP_200_OK)


class MockPayment(APIView):
    def post(self, request):
        # Extract payment data from the request
        price = request.data.get('price', 0)
        quantity = request.data.get('quantity', 0)

        # Add some basic validation if needed

        # Simulate a successful payment
        payment_status = {
            'success': True,
            'message': f'Payment for {quantity} ticket(s) at ${price} each was successful.'
        }
        return Response(payment_status)
