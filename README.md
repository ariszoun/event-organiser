# Check swagger doc
- python manage.py runserver
- Swagger: http://localhost:8000/swagger/
- ReDoc: http://localhost:8000/redoc/

## DATABASE:
docker run -e 
POSTGRES_USER=dbusername -e 
POSTGRES_PASSWORD=dbuserpass -e 
POSTGRES_DB=eventdb -p 
5433:5432 --name postgres-container 
-d library/postgres

User: This entity would store information about the app's users, including their login credentials and whether they're a host or attendee.

Event: This entity would represent the events that hosts create and attendees can book tickets for. It could include attributes like the event's name, date and time, location, category, and ticket price.

Ticket: This entity would represent the tickets that attendees purchase for events. It could include attributes like the ticket's price, the number of tickets purchased, and a foreign key to the corresponding event.

Category: This entity would represent the different categories that events can belong to, such as private events, corporate events, sponsored or fundraising events, and educational events.

## For model changes:
python manage.py makemigrations events
python manage.py migrate

## FILESTRUCTURE:
events/
    __init__.py
    models/
        __init__.py
        category.py
        event.py
        ticket.py
        custom_user.py
